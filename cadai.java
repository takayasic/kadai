import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class cadai<total> {
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane info;
    private javax.swing.JLabel JLabel;
    private JLabel totalinfo;

    int total = 0;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food +" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + " ! It will be served as soon as possible.");
            total += 100;
            info.setText(info.getText()+food+"\n");
            totalinfo.setText("Total      "+total+"yen");

        }
    }







    public cadai() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");

            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout? ",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(x == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total +" yen.");
                    total = 0;
                    totalinfo.setText("Total      " + total + " yen");
                    info.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("cadai");
        frame.setContentPane(new cadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
