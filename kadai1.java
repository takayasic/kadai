import java.util.Scanner;

public class kadai1 {
    static void order(String food){
        System.out.println("You have order " + food);
        System.out.println("Thank you");
    }
    public static void main(String[] args) {
        System.out.println("what would you like to order");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.println("Your order [1-3] : ");
        Scanner x = new Scanner(System.in);
        int number = x.nextInt();
        x.close();
        if (number == 1) {
           order("Tempura");
        }

        if (number == 2) {
            order("Ramen");
        }

        if (number == 3) {
            order("Udon");
        }
    }
}
